import System.Environment 

main = do 
    args <- getArgs
    if null args
      then mapM putStrLn (repeat "y")
	  else mapM putStrLn (cycle [(foldl (++) "" (init (concatMap (\s -> [s, " "]) args)))])
