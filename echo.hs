import System.Environment 

main = do 
    args <- getArgs
    if null args
      then putStrLn ""
	  else putStrLn (foldl (++) "" (init (concatMap (\s -> [s, " "]) args)))
